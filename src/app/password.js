var crypto = require('crypto');

var password = {
    generateSalt: function (length) {
        return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
    },
    hashPassword: function (pw, salt) {
        var hash = crypto.createHmac('sha512', salt);
        hash.update(pw);
        var value = hash.digest('hex');
        return value;
    }
}

module.exports = password;