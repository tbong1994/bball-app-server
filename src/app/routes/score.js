var express = require('express');
var router = express.Router();
var path = require('path');
//http requests
var db = require('../database-service');
var password = require('./../password');
var today = require('./../today');

router.put('/update', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var team1Score = req.body.team1Score;
    var team2Score = req.body.team2Score;
    var date = req.body.date;
    /*TODO: get scores and update scores. Then update players' ratings based on the scores */
    query = 'UPDATE score \
            SET score = ?, date = ? WHERE team = 1;\
            UPDATE score \
            SET score = ?, date = ? WHERE team = 2;\
            INSERT INTO last_game_stats(id,team,date,score)\
            SELECT p.id, rat.team, rat.last_played_date, sc.score FROM players p\
            LEFT JOIN rating_and_team rat ON rat.id = p.id LEFT JOIN score sc on rat.last_played_date = sc.date && rat.team = sc.team\
            WHERE sc.score IS NOT NULL\
            ON DUPLICATE KEY UPDATE team = rat.team, date = rat.last_played_date, score = sc.score';
    db.query(query, [team1Score, date, team2Score, date, team1Score, date, team2Score, date], (error, result) => {
        if (error) {
            throw err;
        }
        updatePlayerScore(team1Score, team2Score, date, res);
    });
});

/*UPDATE PLAYERS' RATINGS */
function updateRatings(team1Score, team2Score, res) {
    var query;
    if (team1Score === team2Score) { // a tie
        return res.send(true);
    }
    query = team1Score > team2Score ?
        'UPDATE rating_and_team \
                SET rating = rating + 1\
                WHERE team = 1 \
                AND last_played_date =  date_format(CURDATE(), "%m-" "%d-" "%Y");' :
        'UPDATE rating_and_team \
                SET rating = rating + 1\
                WHERE team = 2 AND\
                last_played_date =  date_format(CURDATE(), "%m-" "%d-" "%Y")';
    db.query(query, (error, result) => {
        if (error) {
            throw error;
        }
        res.send(result);
    });
}

function updatePlayerScore(team1Score, team2Score, date, res) {
    var query;
    if (team1Score === team2Score) {
        /**
         * TODO: update tie score
         */

        // query = 'UPDATE player_score SET tie = tie + 1 WHERE last_played_date = ' + date;
        return true;
    } else {
        /**
         * TODO: reduce this to 1 line query per execution
         */
        query = team1Score > team2Score ?
            'UPDATE player_score SET win = win + 1 where team = 1 and last_played_date = ?;\
            UPDATE player_score SET loss = loss + 1 where team = 2 and last_played_date = ?' :
            'UPDATE player_score SET win = win + 1 where team = 2 and last_played_date = ?;\
            UPDATE player_score SET loss = loss + 1 where team = 1 and last_played_date = ?';
    }

    db.query(query, [date, date], (error, result) => {
        if (error) {
            throw error;
        }
        updateRatings(team1Score, team2Score, res);
    });
}

router.get('/last/game', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    query = 'SELECT sc.team, sc.score, sc.date, p.firstname FROM\
            score sc LEFT JOIN last_game_stats lgs ON sc.team = lgs.team && sc.date = lgs.date\
            LEFT JOIN players p ON lgs.id = p.id\
            ORDER BY sc.team';

    db.query(query, (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

router.get('/last-update-date', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    query = 'SELECT date FROM score limit 1';
    db.query(query, (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

module.exports = router;