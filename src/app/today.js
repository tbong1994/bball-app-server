var today = {
    getTodaysDate: function getTodaysDate() {
        var today = new Date();
        var day = today.getDate();
        var month = today.getMonth() + 1;
        var year = today.getFullYear();
        month < 10 ? month = '0' + month : month = month;
        return month + '-' + day + '-' + year;
    }
}

module.exports = today;