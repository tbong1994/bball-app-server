var express = require('express');
var router = express.Router();
var path = require('path');
// var nodemailer = require('nodemailer');
// var email = require('./email/email');
var db = require('../database-service');
var password = require('./../password');
var today = require('./../today');

/*GRAB EVERYONE */
router.get('/', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    query = 'SELECT pl.id as ID, pl.firstname, pl.lastname, pl.email, pl.is_admin as isAdmin, pp.imgurl, v.answer,\
             v.answerdate, r.rating, r.stats, r.team, r.last_played_date, h.height, s.speed \
             FROM players pl \
             LEFT JOIN profilepic pp ON pp.id = pl.id \
             LEFT JOIN vote v ON pl.id = v.id \
             LEFT JOIN rating_and_team r ON pl.id = r.id \
             LEFT JOIN height h ON pl.id = h.id\
             LEFT JOIN speed s ON pl.id = s.id';
    db.query(query, (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

// get player's score by id
router.get('/get/score', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const playerID = req.query.id;
    const query = 'SELECT win,loss FROM player_score AS score where id = ?';

    db.query(query, [playerID], (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});


/*DELETE A SINGLE PLAYER */
router.delete('/delete', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const playerID = req.query.id;
    const query = 'DELETE p,l,pr,v,r,pl FROM pin p LEFT JOIN last_game_stats l ON p.id = l.id LEFT JOIN profilepic pr ON p.id = pr.id \
                    LEFT JOIN vote v ON p.id = v.id LEFT JOIN rating_and_team r on p.id = r.id LEFT JOIN players pl ON p.id = pl.id WHERE p.id = ?;'
    db.query(query, [playerID], (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

/*EDIT PLAYER'S PROFILE */
router.put('/edit', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const playerID = req.query.id;
    const newFirstname = req.body.firstname;
    const newLastname = req.body.lastname;
    const newEmail = req.body.email;
    const newImgurl = req.body.imgurl;

    editQuery = 'UPDATE players pl LEFT JOIN vote v ON pl.id = v.id \
    LEFT JOIN profilepic pro ON pl.id = pro.id \
    SET pl.firstname= ?, pl.lastname =?, pl.email =?, v.lastname =?, pro.imgurl = ? \
    WHERE pl.id = ?';
    db.query(editQuery, [newFirstname, newLastname, newEmail, newLastname, newImgurl, playerID], (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

/*SAVE USER VOTE */
router.put('/vote/save', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    var playerID = req.body[0];
    query = 'UPDATE vote SET answer = ?, answerdate =  date_format(CURDATE(), "%m-" "%d-" "%Y"),\
            answer_time = current_timestamp()\
            WHERE id = ?';
    db.query(query, [1, playerID], (err, result) => {
        if (err) {
            res.send(err);
            throw err;
        }
        updateAlternatePlayer(res);
    });
});

// update seventh player
function updateAlternatePlayer(res) {
    // update whenever new player signs in
    query = 'DELETE from alternate_player;\
             INSERT INTO alternate_player(player_id, lastname, answer_time) \
             SELECT v.id, v.lastname, v.answer_time from vote v\
             WHERE v.answerdate = date_format(curdate(),"%m-" "%d-" "%Y") AND answer = 1\
             ORDER BY v.answer_time DESC\
             LIMIT 1\
             ON DUPLICATE KEY\
             UPDATE answer_time = v.answer_time';

    db.query(query, (err, result) => {
        if (err) {
            res.send(err);
            throw err;
        }
        res.send(result);
    });
}

// get seventh player
router.get('/get/alternate', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    query = 'SELECT player_id as id FROM alternate_player\
             ORDER BY answer_time DESC\
             LIMIT 1'; // grab the top row(latest timestamp) only
    db.query(query, (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

router.put('/update-teams', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    let queryResult;
    for (var i = 0; i < req.body.length; i++) {
        const userID = req.body[i].ID;
        const userTeam = req.body[i].team;
        const query = 'UPDATE rating_and_team r\
                        INNER JOIN player_score p ON r.id = p.id\
                        SET r.team = ?, p.team =?, r.last_played_date = date_format(CURDATE(), "%m-" "%d-" "%Y"),\
                        p.last_played_date = date_format(CURDATE(), "%m-" "%d-" "%Y")\
                        WHERE r.id = ?';
        db.query(query, [userTeam, userTeam, userID], (err, result) => {
            if (err) {
                throw err;
            }
            queryResult = result; //gets only the last query's response. ==> getting all the responses necessary?
        });
    }
    res.send(queryResult);
});

/*REMOVE PLAYERS FROM TODAY'S PLAYER LIST */
router.delete('/remove-from-todays-playlist', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const userID = req.query.id;
    query = 'UPDATE vote SET answer = ? WHERE id=?;\
            UPDATE rating_and_team SET last_played_date = "" WHERE id = ?;\
            DELETE from alternate_player WHERE player_id = ?';
    db.query(query, [0, userID, userID, userID], (err, result) => {
        if (err) {
            throw err;
        }
        updateAlternatePlayer(res);
    });
});

/*PIN AUTHENTICATION*/
router.get('/pin', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const enteredPin = req.query.pin;
    const playerID = req.query.id; //req.query corresponds to the URLSearchParams query sent from the web server.

    query = 'SELECT salt, hash FROM pin WHERE id = ?';
    db.query(query, [playerID], (err, result) => {
        if (err) {
            throw err;
        }
        const salt = result[0].salt;
        const hash = result[0].hash;
        if (validatePin(enteredPin, salt, hash)) {
            const authenticatedData = JSON.stringify({
                /**
                 * TODO: create a data constant npm module and use that to serialize data instead of hard coding it
                 */
                'authenticated': true,
                'token': generateToken()
            });
            res.send(authenticatedData);
        } else {
            res.send({
                'authenticated': false
            });
        }
    });
});

function validatePin(pin, salt, hash) {
    hashWithProvidedPin = password.hashPassword(pin, salt);
    debugger;
    if (hashWithProvidedPin === hash) {
        return true;
    }
    return false;
}

router.put('/update-pin', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const userID = req.body.id;
    const pin = req.body.pin;

    const salt = password.generateSalt(10);
    const hashValue = password.hashPassword(pin, salt);

    const query = 'UPDATE pin SET hash = ?, salt =? WHERE id = ?';
    db.query(query, [hashValue, salt, userID], (err, result) => {
        if (err) {
            res.send(JSON.stringify({
                'isUpdated': false
            }));
            throw err;
        }
        res.send(JSON.stringify({
            'isUpdated': true
        }));
    });
});

/*REMOVE PLAYERS FROM TODAY'S PLAYER LIST */
router.delete('/remove-from-todays-playlist', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const userID = req.query.id;
    query = 'UPDATE vote SET answer = ? WHERE id=?;\
            UPDATE rating_and_team SET last_played_date = "" WHERE id = ?;\
            DELETE from alternate_player WHERE player_id = ?';
    db.query(query, [0, userID, userID, userID], (err, result) => {
        if (err) {
            throw err;
        }
        updateAlternatePlayer(res);
    });
});

/*GRAB players who have said yes today */
router.get('/playingtoday', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    query = 'SELECT pl.firstname, pl.lastname, pp.imgurl \
    FROM players pl \
    LEFT JOIN profilepic pp ON pp.id = pl.id \
    LEFT JOIN vote v ON v.id=pl.id \
    WHERE answer="yes" AND answerdate = ?'; //get everyone from the database.
    db.query(query, [getTodaysDate()], (err, response) => {
        if (err) {
            console.log(err.message);
            return;
        }
        res.send(response);
    });
});

function getTodaysDate() {
    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    return month + '/' + day + '/' + year;
}

function generateToken() {
    var text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#!@$%&~";
    for (var i = 0; i < 20; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
module.exports = router;