var express = require('express');
var router = express.Router();
var path = require('path');
//http requests
var db = require('../database-service');
var password = require('./../password');
var today = require('./../today');
/*Create a user */
router.post('/create', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const profPic = req.body.imgurl;
    const pin = req.body.pin;

    const salt = password.generateSalt(10);
    const hashValue = password.hashPassword(pin, salt);
    /*TODO: Refactor these quries so they are in 1 query*/
    query = 'INSERT INTO players(firstname,lastname,email) values (?,?,?);\
    INSERT INTO pin(id,hash,salt) SELECT LAST_INSERT_ID(),?,?;\
    INSERT INTO profilepic(id, imgurl) SELECT LAST_INSERT_ID(),?;\
    INSERT INTO vote(id,lastname,answer,answerdate) SELECT LAST_INSERT_ID(), lastname, ?, date_format(CURDATE(), "%m-" "%d-" "%Y") FROM players WHERE id = LAST_INSERT_ID();\
    INSERT INTO rating_and_team(id,firstname,rating,stats) values (LAST_INSERT_ID(),?,?,?);\
    INSERT INTO player_score(id) value(LAST_INSERT_ID());\
    INSERT INTO last_game_stats(id) value(LAST_INSERT_ID());';
    db.query(query, [firstname, lastname, email, hashValue, salt, profPic, 0, firstname, 3, 5], (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

/*GET ALL PROFILE PICTURES */
router.get('/profilepictures', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    query = 'select imgurl from pictures';
    db.query(query, (err, result) => {
        if (err) {
            throw err;
        }
        res.send(result);
    });
});

module.exports = router;